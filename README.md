# UI Dev Technical Test - Dishwasher App

## Brief

Your task is to create a website that will allow customers to see the range of dishwashers John Lewis sells. This app will be a simple to use and will make use of existing John Lewis APIs.

We have started the project, but we'd like you to finish it off to a production-ready standard.

### Product Grid

When the website is launched, the customer will be presented with a grid of dishwashers that are currently available for customers to buy.

For this exercise we’d be looking at only displaying the first 20 results returned by the API.

### Product Page

When a dishwasher is clicked, a new screen is displayed showing the dishwasher’s details.

### Designs

In the `/designs` folder you will find 3 images that show the desired screen layout for the app

- product-page-portrait.png
- product-page-landscape.png
- product-grid.png

## Things we're looking for

- The website should be fully responsive, working across device sizes. We've provided you with some ipad-sized images as a guide.
- The use of third party code/SDKs is allowed, but you should be able to explain why you have chosen the third party code.
- We’d like to see a TDD approach to writing the app, we've included a Jest setup or you can use your preferred option.
- Put all your assumptions, notes and instructions into your GitHub README.md.
- We’re looking for a solution that's as close to the designs as possible.
- We'll be assessing your coding style, how you've approached this task and whether you've met quality standards on areas such as accessibility, security and performance.
- You shouldn't spend more than 3 hours on this task.

---

## Getting Started

- `Fork this repo` into your own GitLab namespace (you will need a GitLab account).
- Install the dependencies using `npm i`
- Run the development server with `nmp run dev`
- Open [http://localhost:3000](http://localhost:3000) with your browser.

# Test Process

## Observations

- `fetch` function didn't seem to work in `getServerSideProps`, while `node-fetch`, `getServerSideProps` never resolves. Upon investigating, it seems that the endpoint requires a User-Agent header to be set to return the content, as it worked in browser, and so used an appropriate User-Agent string.
