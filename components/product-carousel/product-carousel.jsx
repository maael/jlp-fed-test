import * as React from "react";
import styles from "./product-carousel.module.scss";

const ProductCarousel = ({ images, altText }) => {
  const [activeIdx, setActiveIdx] = React.useState(0);
  return (
    <div className={styles.productCarousel} aria-pretend="true">
      <img
        src={images[activeIdx]}
        alt={`Image ${[activeIdx + 1]} of ${altText}`}
        style={{ width: "100%", maxWidth: "500px" }}
      />
      <div className={styles.productCarouselDots} role="radiogroup">
        {images.map((img, i) => (
          <input
            role="radio"
            aria-checked={i === activeIdx}
            key={img}
            type="radio"
            checked={i === activeIdx}
            onChange={() => setActiveIdx(i)}
          />
        ))}
      </div>
    </div>
  );
};

export default ProductCarousel;
