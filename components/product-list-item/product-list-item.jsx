import Link from "next/link";
import styles from "./product-list-item.module.scss";

const ProductListItem = ({ image, price, description, id, currency }) => {
  return (
    <Link
      href={{
        pathname: "/product-detail/[id]",
        query: { id },
      }}
    >
      <a className={styles.link}>
        <div className={styles.content}>
          <div>
            <img src={image} alt={description} style={{ width: "100%" }} />
          </div>
          <div>{description}</div>
          <div className={styles.price}>
            {Intl.NumberFormat("en-GB", { style: "currency", currency }).format(
              price
            )}
          </div>
        </div>
      </a>
    </Link>
  );
};

export default ProductListItem;
