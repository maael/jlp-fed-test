import * as React from "react";
import { render, screen } from "@testing-library/react";
import ProductListItem from "./product-list-item";

const mockProducts = require("../../mockData/data.json").products;

describe("Product List Item", () => {
  it("should render with the required details", () => {
    const product = mockProducts[0];
    const item = {
      image: product.image,
      price: product.price.now,
      description: product.title,
      id: 0,
      currency: product.price.currency,
    };
    render(<ProductListItem {...item} />);
    expect(screen.getByText(item.description)).toBeTruthy();
    expect(screen.getByText(`£${item.price}`)).toBeTruthy();
    expect(screen.getByAltText(item.description).src).toBe(
      `http:${item.image}`
    );
  });
});
