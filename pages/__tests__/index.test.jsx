import * as React from "react";
import { render, screen } from "@testing-library/react";
import Home from "..";

const mockProducts = require("../../mockData/data.json").products;

describe("Product List Item", () => {
  it("should render 20 products", () => {
    render(<Home data={{ products: mockProducts }} />);
    expect(screen.getAllByRole("link")).toHaveLength(20);
  });
});
