import Head from "next/head";
import styles from "./index.module.scss";
import ProductListItem from "../components/product-list-item/product-list-item";

export async function getServerSideProps() {
  const response = await fetch(
    "https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI",
    {
      headers: {
        "User-Agent":
          "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:95.0) Gecko/20100101 Firefox/95.0",
      },
    }
  );
  const data = await response.json();
  return {
    props: {
      data: data,
    },
  };
}

const Home = ({ data }) => {
  let items = data.products;
  return (
    <div>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <div>
        <h1 className={styles.header}>Dishwashers ({items.length})</h1>
        <div className={styles.content}>
          {items.slice(0, 20).map((item) => (
            <ProductListItem
              key={item.productId}
              id={item.productId}
              image={item.image}
              description={item.title}
              currency={item.price.currency}
              price={item.price.now}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Home;
