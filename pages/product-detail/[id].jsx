import * as React from "react";
import Link from "next/link";
import ProductCarousel from "../../components/product-carousel/product-carousel";
import styles from "./product-detail.module.scss";
import cls from "classnames";

export async function getServerSideProps(context) {
  const id = context.params.id;
  const response = await fetch(
    "https://api.johnlewis.com/mobile-apps/api/v1/products/" + id,
    {
      headers: {
        "User-Agent":
          "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:95.0) Gecko/20100101 Firefox/95.0",
      },
    }
  );
  const data = await response.json();

  return {
    props: { data },
  };
}

const ProductKeyDetails = ({ data }) => (
  <div className={styles.keyDetails}>
    <h1 className={styles.price}>
      {Intl.NumberFormat("en-GB", {
        style: "currency",
        currency: data.price.currency,
      }).format(data.price.now)}
    </h1>
    <p className={styles.offer}>{data.displaySpecialOffer}</p>
    <p
      className={cls(styles.services, "prose")}
      dangerouslySetInnerHTML={{
        __html: data.additionalServices.includedServices,
      }}
    />
  </div>
);

const ProductDetail = ({ data }) => {
  const [readMore, setReadMore] = React.useState(false);
  return (
    <div>
      <h1 className={styles.heading}>
        <Link href="/">
          <a>
            <ChevronLeftIcon className={styles.backIcon} />
          </a>
        </Link>
        <div dangerouslySetInnerHTML={{ __html: data.title }} />
      </h1>
      <div className={styles.cols}>
        <div className={styles.main}>
          <div>
            <ProductCarousel
              images={data.media.images.urls}
              altText={data.title}
            />
            <div className={styles.separator} />
            <div className={styles.wrapper}>
              <ProductKeyDetails data={data} />

              <h3 className={styles.productInfoHeading}>Product information</h3>
              <div className={styles.productInfo}>
                <div
                  className={cls("prose", { [styles.limited]: !readMore })}
                  dangerouslySetInnerHTML={{
                    __html: data.details.productInformation,
                  }}
                />

                <p className={styles.productCode}>Product code: {data.code}</p>
              </div>
              <button
                className={styles.readMore}
                onClick={() => setReadMore((r) => !r)}
              >
                <span>Read {readMore ? "less" : "more"}</span>
                <ChevronLeftIcon />
              </button>
              <h3 className={styles.specificationHeading}>
                Product specification
              </h3>
              {data.details.features[0].attributes.map((item) => (
                <div key={item.name} className={styles.feature}>
                  <div>{item.name}</div>
                  <div>{item.value}</div>
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className={styles.landscapeSidebar}>
          <ProductKeyDetails data={data} />
        </div>
      </div>
    </div>
  );
};

function ChevronLeftIcon({ className }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      stroke="currentColor"
      className={className}
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M15 19l-7-7 7-7"
      />
    </svg>
  );
}

export default ProductDetail;
